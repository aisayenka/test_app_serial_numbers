@extends('layouts.main')

@section('title', 'Welcome');

@section('content')
  <div class="col-lg-6">
    <h4>Checkout <a href="{{ url("/goats/") }}" class="btn btn-primary">Goats</a></h4>
  </div>
  <div class="col-lg-6">
    <h4>Checkout <a href="{{ url("/sheeps/") }}" class="btn btn-primary">Sheeps</a></h4>
  </div>
@endsection
