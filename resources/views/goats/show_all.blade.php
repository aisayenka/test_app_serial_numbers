@extends('layouts.main')

@section('title', 'Goats. Show All')

@section('content')
  <div class="col-lg-12">
    <a class="btn btn-primary" href="{{ url("goats/add") }}">Add new record</a>
    <table class="datatable" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Serial Number</th>
          <th>Created</th>
          <th>Updated</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Serial Number</th>
          <th>Created</th>
          <th>Updated</th>
          <th>Actions</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach($records as $record)
          <tr>
            <td>{{ $record->id }}</td>
            <td>{{ $record->name }}</td>
            <td>{{ $record->serial_number }}</td>
            <td>{{ $record->created_at }}</td>
            <td>{{ $record->updated_at }}</td>
            <td>
              <a href="{{ url('/goats/'.$record->id.'/show') }}" class="btn btn-primary">Show</a>
              <a href="{{ url('/goats/'.$record->id.'/delete') }}" class="btn btn-danger">Delete</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
