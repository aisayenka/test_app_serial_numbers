@extends('layouts.main')

@section('title', 'Goats. Show (ID: '.$record->id.')');

@section('content')
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
        <b>ID:</b>
      </div>
      <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6">
        <p>{{ $record->id }}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
        <b>Name:</b>
      </div>
      <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6">
        <p>{{ $record->name }}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
        <b>Serial Number:</b>
      </div>
      <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6">
        <p>{{ $record->serial_number }}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
        <b>Created:</b>
      </div>
      <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6">
        <p>{{ $record->created_at }}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
        <b>Updated:</b>
      </div>
      <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6">
        <p>{{ $record->updated_at }}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <a href="{{ url('/goats/'.$record->id.'/edit') }}" class="btn btn-warning">Edit<a> | <a href="{{ url('/goats/'.$record->id.'/delete') }}" class="btn btn-danger">Delete<a>
      </div>
    </div>
  </div>
@endsection
