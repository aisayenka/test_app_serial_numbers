@extends('layouts.main')

@section('title', 'Goats. Add new record')

@section('content')
  @include("goats._form")
@endsection
