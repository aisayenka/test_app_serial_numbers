@extends('layouts.main')

@section('title', 'Goats. Update Record (ID '.$record->id.')')

@section('content')
  @include("goats._form")
@endsection
