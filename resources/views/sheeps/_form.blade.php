{{ Form::model($record, ['url' => $url_to, 'method' => 'post', "enctype" => "multipart/form-data"]) }}
  <div class="form-group">
    {{ Form::label('name', 'Name:', ["class" => "col-sm-2 control-label"]) }}
    <div class="col-sm-10">
      {{ Form::text('name', $record->name, ["class" => "form-control"]) }}
    </div>
  </div>
  {{ Form::submit($submit_text, ["class" => "btn btn-primary"]) }}
  {{ Form::reset("Reset", ["class" => "btn btn-warning"]) }}
{{ Form::close() }}
