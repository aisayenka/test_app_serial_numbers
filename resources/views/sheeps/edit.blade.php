@extends('layouts.main')

@section('title', 'Sheeps. Update Record (ID '.$record->id.')')

@section('content')
  @include("sheeps._form")
@endsection
