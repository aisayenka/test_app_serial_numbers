@if(session('message_success'))
<div class="alert alert-success">
  <strong>Success!</strong> {{ session('message_success') }}
</div>
@endif

@if(session('message_error'))
<div class="alert alert-danger">
  <strong>Error!</strong> {{ session('message_error') }}
</div>
@endif

@if(isset($errors) && count($errors) > 0)
<div class="alert alert-danger">
  @if($errors->all())
      @foreach ($errors->all() as $error)
          {{ $error }}.
      @endforeach
  @endif
</div>
@endif
