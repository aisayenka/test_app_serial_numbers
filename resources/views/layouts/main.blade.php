<!DOCTYPE html>

<html lang="en">
    <head>
        <title>@yield('title', 'Test Task | Alex Isayenka')</title>

        @include('_includes._general')
    </head>

    <body>
      @include("_includes._nav_bar")
      <div class="container theme-showcase" role="main">
        <div class="page-header">
          <h1>@yield('title', 'Test Task | Alex Isayenka')</h1>
        </div>
        @include("_includes._errors")
        @yield("content")
      </div>
    </body>
</html>
