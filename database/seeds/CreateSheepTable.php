<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CreateSheepTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Setting up variables used in the code
      $serial_number = "AAAAAAAAAAAAA";
      $data = [];


      // Generating first 100 Sheep records information
      for($i = 0; $i < 100; $i++) {
        if($i !== 0) $serial_number++;
        $data[] = [
          'name' => "Sheep ".($i+1),
          'serial_number' => $serial_number,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ];
      }

      // Inserting data into the sheeps table
      DB::table('sheeps')->insert($data);
    }
}
