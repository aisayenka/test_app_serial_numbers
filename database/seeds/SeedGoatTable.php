<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SeedGoatTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       // Setting up variables used in the code
       $serial_number = "AAAAAAAAAAAAA";
       $data = [];


       // Generating first 100 Goat records information
       for($i = 0; $i < 100; $i++) {
         if($i !== 0) $serial_number++;
         $data[] = [
           'name' => "Goat ".($i+1),
           'serial_number' => $serial_number,
           'created_at' => Carbon::now(),
           'updated_at' => Carbon::now(),
         ];
       }

       // Inserting data into the goats table
       DB::table('goats')->insert($data);
     }
}
