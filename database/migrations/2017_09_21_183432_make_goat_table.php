<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeGoatTable extends Migration
{
  public function up()
  {
    // Creating sheeps table
    Schema::create('goats', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->string('serial_number')->unique();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  public function down()
  {
    Schema::dropIfExists('goats');
  }
}
