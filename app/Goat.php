<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Goat extends Model
{
    use SoftDeletes;

    protected $table = "goats";

    protected $fillable = [
      "name",
      "serial_number"
    ];
}
