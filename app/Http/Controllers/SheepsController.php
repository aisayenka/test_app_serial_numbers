<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sheep;

class SheepsController extends Controller
{
  public function show_all() {
    // Getting all the records
    $records = Sheep::all();
    return view("sheeps.show_all", [ "records" => $records ]);
  }

  public function show_record($id) {
    // Finding record
    $record = Sheep::findOrFail($id);
    return view("sheeps.show", [ "record" => $record ]);
  }

  public function show_add_form() {
    // New record since we are using one form template for add and edit forms
    $record = new Sheep();
    return view("sheeps.add", [ "record" => $record, "url_to" => url("sheeps/add"), "submit_text" => "Create Sheep Record" ]);
  }

  public function add_record(Request $request) {
    $this->validate($request, [
        'name' => 'required'
    ]);
    // Getting previous number
    $previous_record_serial_number = Sheep::all()->last()->serial_number;
    $previous_record_serial_number++;

    // Setting up the values for the new record
    $input = $request->input();
    $input["serial_number"] = $previous_record_serial_number;

    // Creating the record
    $record = Sheep::create($input);

    // Redirecting the show page of the new record
    return redirect("sheeps/".$record->id."/show")->with("message_success", "The record has been created successfully");
  }

  public function show_edit_form($id) {
    // New record since we are using one form template for add and edit forms
    $record = Sheep::findOrFail($id);
    return view("sheeps.edit", [ "record" => $record, "url_to" => url("sheeps/".$id."/edit"), "submit_text" => "Update Sheep Record" ]);
  }

  public function edit_record($id, Request $request) {
    $this->validate($request, [
        'name' => 'required'
    ]);
    // We dont need to get the serial number since it's unmutable
    // Getting the record
    $record = Sheep::findOrFail($id);

    // Setting up the values for the new record
    $input = $request->input();

    // Creating the record
    $record->update($input);

    // Redirecting the show page of the new record
    return redirect("sheeps/$id/show")->with("message_success", "The record has been updated successfully");
  }

  public function delete_record($id) {
    // Getting record
    $record = Sheep::findOrFail($id);

    // Deleting the record
    $record->delete();

    // Redirecting the show all page
    return redirect("sheeps")->with("message_success", "The record has been deleted successfully");
  }
}
