<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Goat;

class GoatsController extends Controller
{
    public function show_all() {
      // Getting all the records
      $records = Goat::all();
      return view("goats.show_all", [ "records" => $records ]);
    }

    public function show_record($id) {
      // Finding record
      $record = Goat::findOrFail($id);
      return view("goats.show", [ "record" => $record ]);
    }

    public function show_add_form() {
      // New record since we are using one form template for add and edit forms
      $record = new Goat();
      return view("goats.add", [ "record" => $record, "url_to" => url("goats/add"), "submit_text" => "Create Goat Record" ]);
    }

    public function add_record(Request $request) {
      $this->validate($request, [
          'name' => 'required'
      ]);
      // Getting previous number
      $previous_record_serial_number = Goat::all()->last()->serial_number;
      $previous_record_serial_number++;

      // Setting up the values for the new record
      $input = $request->input();
      $input["serial_number"] = $previous_record_serial_number;

      // Creating the record
      $record = Goat::create($input);

      // Redirecting the show page of the new record
      return redirect("goats/".$record->id."/show")->with("message_success", "The record has been created successfully");
    }

    public function show_edit_form($id) {
      // New record since we are using one form template for add and edit forms
      $record = Goat::findOrFail($id);
      return view("goats.edit", [ "record" => $record, "url_to" => url("goats/".$id."/edit"), "submit_text" => "Update Goat Record" ]);
    }

    public function edit_record($id, Request $request) {
      $this->validate($request, [
          'name' => 'required'
      ]);
      // We dont need to get the serial number since it's unmutable
      // Getting the record
      $record = Goat::findOrFail($id);

      // Setting up the values for the new record
      $input = $request->input();

      // Creating the record
      $record->update($input);

      // Redirecting the show page of the new record
      return redirect("goats/$id/show")->with("message_success", "The record has been updated successfully");
    }

    public function delete_record($id) {
      // Getting record
      $record = Goat::findOrFail($id);

      // Deleting the record
      $record->delete();

      // Redirecting the show all page
      return redirect("goats")->with("message_success", "The record has been deleted successfully");
    }
}
