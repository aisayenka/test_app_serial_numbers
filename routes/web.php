<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Goats
Route::group(['prefix' => 'sheeps'/*, 'middleware' => ['role:authorized_user']*/], function () {
    Route::get("/", 'GoatsController@show_all');
    Route::get("/add", 'GoatsController@show_add_form');
    Route::post("/add", 'GoatsController@add_record');
    Route::get("/{id}/edit", 'GoatsController@show_edit_form');
    Route::post("/{id}/edit", 'GoatsController@edit_record');
    Route::get("/{id}/show", 'GoatsController@show_record');
    Route::get("/{id}/delete", 'GoatsController@delete_record');
});

//Goats
Route::group(['prefix' => 'sheeps'/*, 'middleware' => ['role:authorized_user']*/], function () {
    Route::get("/", 'SheepsController@show_all');
    Route::get("/add", 'SheepsController@show_add_form');
    Route::post("/add", 'SheepsController@add_record');
    Route::get("/{id}/edit", 'SheepsController@show_edit_form');
    Route::post("/{id}/edit", 'SheepsController@edit_record');
    Route::get("/{id}/show", 'SheepsController@show_record');
    Route::get("/{id}/delete", 'SheepsController@delete_record');
});
